import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator()

import RegisterStack from './screens/RegisterStack'

function MyStack(){
  return(
    <Stack.Navigator>
      <Stack.Screen name= "Registro" component={RegisterStack}/>
    </Stack.Navigator>
  )
}
const App = () =>  {

  return (
   <NavigationContainer>
     <MyStack/>
   </NavigationContainer>
  );
};



export default App;
